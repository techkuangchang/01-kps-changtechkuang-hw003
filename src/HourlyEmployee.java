public class HourlyEmployee extends StaffMember {
    private int hoursWorked;
    private double rate;
    private double payment;

    public HourlyEmployee(int id, String name, String address, int hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
        payment = rate * hoursWorked;
    }

    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "-> HourlyEmployee : " + "\n" +
                "ID          : " + id + "\n" +
                "Name        : " + name + "\n" +
                "Address     : " + address + "\n" +
                "HoursWorked : " + hoursWorked +"\n" +
                "Rate        : " + rate +"\n" +
                "Payment     : " + payment;
    }

    @Override
    public double pay() {
        return 0;
    }

}
