import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args){
        Scanner cin = new Scanner(System.in);
        //declare var for expression check
        String check = "\\d";
        String match = "";

        //expression check
        Pattern pattern = Pattern.compile(check);
        Matcher m = pattern.matcher(match);

        //Call Method
        Method method = new Method();

        //initialisations
        method.inDisplay();

        ArrayList<StaffMember> employee = new ArrayList<>();

    while (true){

        //call menu
        method.menu();

        //expression check
        match = cin.next();

        if(match.matches("1")){
            Scanner scan = new Scanner(System.in);

            System.out.println("============| INSERT Employee INFO |===========");
            System.out.println("1. Volunteer Employee");
            System.out.println("2. Hourly Employee");
            System.out.println("3. Salaried Employee");
            System.out.println("4. Back");
            System.out.println("---------------------------------------");
            System.out.print("=> Choose option(1-4) : ");
            try {
                match = cin.next();
                switch (match){
                    case "1" :
                        method.inputVolunteerEmp();
                        break;
                    case "2" :
                        method.inputHourlyEmp();
                        break;
                    case "3" :
                        method.inputSalariedEmp();
                        break;
                    case "4" :
                        method.menu();
                        break;
                    default:
                        System.out.println("=> Invalid Input..!");
                        break;
                }
            }catch (Exception e){
            System.out.println("Please Only The Rule!");
            method.promptEnterKey();
         }
        }
        else if (match.matches("2")){
            System.out.println("============| EDIT INFO |===========");
            method.edit();
        }
        else if(match.matches("3")){
            System.out.println("============| REMOVE INFO |===========");
            method.remove();
        }
        else if (match.matches("4")){
            System.out.println("============| REMOVE ALL INFO |===========");
            method.removeAll();
        }
        else if (match.matches("5")){
            System.out.println("-> System is closed! Thanks for using me. (^-^) Good Bye! (^-^)");
            System.exit(0 );
        }
        else {
            System.out.println("Please Only Number (1-4)...!");
            method.promptEnterKey();
        }
    }
    }
}
