public class SalariedEmployee extends StaffMember {

    private double salary;
    private double bonus;
    private double payment;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
        payment = salary + bonus;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "-> SalariedEmployee : " + "\n" +
                "ID          : " + id + "\n" +
                "Name        : " + name + "\n" +
                "Address     : " + address + "\n" +
                "Salary      : " + salary + "\n" +
                "Bonus       : " + bonus + "\n" +
                "Payment     : " + payment;
    }

    @Override
    public double pay() {
        return 0;
    }


}
