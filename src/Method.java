import java.util.ArrayList;
import java.util.*;
import java.util.Scanner;

public class Method {
    Scanner cin = new Scanner(System.in);
    //Parent var
    int id;
    String name = "" , address;
    //var for salary employee
    double salary, bonus;
    //var for hourly employee
    int rate;
    int hoursWorked;

    //Declaring ArrayList
    ArrayList<StaffMember> employee = new ArrayList<>();

    /********************** Remove Employee **************************/
    public void removeAll(){
        try {
            employee.clear();
            System.out.println("=> Deleted All Employee Successfully");
            promptEnterKey();
            show();
        }catch (Exception e){
            System.out.print("Your removing is error...!");
        }
    }

    public void remove(){
        try {
            validateInputID("=> Enter Employee ID to Remove  :");

            Iterator<StaffMember> itr = employee.iterator();

            while (itr.hasNext()){
                StaffMember removestaff = itr.next();
                if (removestaff.getId() == id){
                    itr.remove();
                    System.out.println("Remove Successfully...");
                    promptEnterKey();
                }
            }
            show();
        }catch (Exception e){
            System.out.print("");
        }
    }

    /********************** Update Volunteer Employee **************************/
    public void updateVolEmp(StaffMember staffMember){
        try {
            System.out.println("============ NEW INFORMATION OF STAFF MEMBER ==========");
            System.out.print("=> Enter Staff Member's Name     : ");
            staffMember.setName(cin.next());
        }catch (Exception e){
            System.out.println("Please Enter Following By Rule!");;
            promptEnterKey();
        }
    }

    /********************** Update Salary Employee **************************/
    public void updateSalEmp(StaffMember staffMember){
        try{
        System.out.println("============ NEW INFORMATION OF STAFF MEMBER ==========");
        System.out.print("=> Enter Staff Member's Name     : ");
        staffMember.setName(cin.next());
        System.out.print("=> Enter New Salary              : ");
        ((SalariedEmployee) staffMember).setSalary(cin.nextDouble());
        System.out.print("=> Enter New Bonus               : ");
        ((SalariedEmployee) staffMember).setBonus(cin.nextDouble());
        }catch (Exception e){
            System.out.println("Please Enter Following By Rule!");;
            promptEnterKey();
        }
    }

    /********************** Update Hourly Employee **************************/
    public void updateHouEmp(StaffMember staffMember){
        try {
        System.out.println("============ NEW INFORMATION OF STAFF MEMBER ==========");
        System.out.print("=> Enter Staff Member's Name     : ");
        staffMember.setName(cin.next());
        System.out.print("=> Enter New Hours Worked        : ");
        ((HourlyEmployee) staffMember).setHoursWorked(cin.nextInt());
        System.out.print("=> Enter New Rate                : ");
        ((HourlyEmployee) staffMember).setRate(cin.nextDouble());
        }catch (Exception e){
            System.out.println("Please Enter Following By Rule!");;
            promptEnterKey();
        }
    }

    /********************** Edit Employee **************************/
    public void edit(){
        try {
        validateInputID("=> Enter Employee ID to Update  :");
        for (StaffMember staffMember : employee) {
            if (id == staffMember.getId()) {
                System.out.println(staffMember);
                if (staffMember instanceof Volunteer) {
                    updateVolEmp(staffMember);
                    Comparator<StaffMember> sortName = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
                    Collections.sort(employee, sortName);
                    show();
                } else if (staffMember instanceof SalariedEmployee) {
                    updateSalEmp(staffMember);
                    Comparator<StaffMember> sortName = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
                    Collections.sort(employee, sortName);
                    show();
                } else if (staffMember instanceof HourlyEmployee) {
                    updateHouEmp(staffMember);
                    Comparator<StaffMember> sortName = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
                    Collections.sort(employee, sortName);
                    show();
                } else {
                    System.out.println("Invalid Input...!");
                }
            }
        }
        }catch (Exception e){
            System.out.print("");
        }
    }

    /********************** Input Hourly Employee **************************/
    public void inputHourlyEmp(){
        try {
        System.out.println("============| ADD HOURLY EMPLOYEE INFO |===========");
        validateInputID("=> Enter Staff Member's ID      : ");
        System.out.print("=> Enter Staff Member's Name    : ");
        name = cin.next();
        System.out.print("=> Enter Staff Member's Address : ");
        address = cin.next();
        System.out.print("=> Enter Hours Worked           : ");
        hoursWorked = cin.nextInt();
        System.out.print("=> Enter Rate                   : ");
        rate = cin.nextInt();
        System.out.println();
        StaffMember staffMemberHou = new HourlyEmployee(id,name,address,hoursWorked,rate);
        employee.add(staffMemberHou);
        Comparator<StaffMember> sortName = (StaffMember s1 , StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
        Collections.sort(employee,sortName);
        show();
        }catch (Exception e){
            System.out.println("Please Enter Following By Rule!");;
            promptEnterKey();
        }
    }

    /********************** Input Salary Employee **************************/
    public void inputSalariedEmp(){
        try {
            System.out.println("============| ADD SALARY EMPLOYEE INFO |===========");
            validateInputID("=> Enter Staff Member's ID      : ");
            System.out.print("=> Enter Staff Member's Name    : ");
            name = cin.next();
            System.out.print("=> Enter Staff Member's Address : ");
            address = cin.next();
            System.out.print("=> Enter Staff's Salary         : ");
            salary = cin.nextDouble();
            System.out.print("=> Enter Staff's Bonus          : ");
            bonus = cin.nextInt();
            System.out.println();
            StaffMember staffMemberSal = new SalariedEmployee(id, name, address, salary, bonus);
            employee.add(staffMemberSal);
            Comparator<StaffMember> sortName = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
            Collections.sort(employee, sortName);
            show();
        }catch (Exception e){
            System.out.println("Please Enter Following By Rule!");;
            promptEnterKey();
        }
    }

    /********************** Input Volunteer Employee **************************/
    public void inputVolunteerEmp(){
        try {
            System.out.println("============| ADD VOLUNTEER EMPLOYEE INFO |===========");
            validateInputID("=> Enter Staff Member's ID      : ");
            System.out.print("=> Enter Staff Member's Name    : ");
            name = cin.next();
            System.out.print("=> Enter Staff Member's Address : ");
            address = cin.next();
            StaffMember staffMemberVol = new Volunteer(id, name, address);
            employee.add(staffMemberVol);
            Comparator<StaffMember> sortName = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
            Collections.sort(employee, sortName);
            show();
        }catch (Exception e){
            System.out.println("Please Enter Following By Rule!");;
            promptEnterKey();
        }
    }

    /********************** Menu Employee **************************/
    public void menu(){
        System.out.println();
        System.out.println("==============> MENU INFO <==================");
        System.out.println("1. Add Employee");
        System.out.println("2. Update Employee");
        System.out.println("3. Remove Employee");
        System.out.println("4. Remove All Employee");
        System.out.println("5. Exit");
        System.out.println("---------------------------------------");
        System.out.print("=> Choose option(1-5) : ");
    }

    public void show() {
        for (StaffMember staffMember : employee) {
            System.out.println("----------------------------------------");
            System.out.println(staffMember);
        }
    }

    /********************** Initialization **************************/
    public void inDisplay(){
        employee = new ArrayList<>();
        StaffMember staffMemberVol = new Volunteer(1, "Sam", "2213 PP");
        StaffMember staffMemberSal = new SalariedEmployee(2, "Dara", "1321 TK", 500, 100);
        StaffMember staffMemberHou = new HourlyEmployee(3, "Carla", "1122 PV", 20,30);
        employee.add(staffMemberVol);
        employee.add(staffMemberSal);
        employee.add(staffMemberHou);
        Comparator<StaffMember> sortName = (StaffMember s1 , StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
        Collections.sort(employee,sortName);
        show();
    }

    /********************** Validate Input ID **************************/

    public int validateInputID(String printString){
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.print(printString);
                id = scanner.nextInt();
                if (id < 1) {
                    scanner = new Scanner(System.in);
                } else {
                    break;
                }
            } catch (Exception e) {
                scanner = new Scanner(System.in);
                //rerun while
            }
        }
        return id;
    }

    public void promptEnterKey() {
        System.out.println("Press \"ENTER\" to continue...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
}
