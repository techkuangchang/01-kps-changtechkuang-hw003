public class Volunteer extends StaffMember{

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public String toString() {
        return "-> Volunteer Employee : " + "\n" +
                "ID          : " + id + "\n" +
                "Name        : " + name + "\n" +
                "Address     : " + address + "\n" +
                "Thanks!";
    }

    @Override
    public double pay() {
        return 0;
    }
}
